// When user1 clicks the 'Send Text' button
$('a.follow').click(function(e) {
e.preventDefault();

// Code to get the base url of the current page (need to pass this data to the /outgoing route with the ajax request)
pathArray = location.href.split( '/' );
protocol = pathArray[0];
host = pathArray[2];
incoming_url = protocol + '//' + host + '/incoming';

// Prompt the user1 for their message for user2
var msgg = prompt("Please enter the message you want to send:");
if (msgg != null) {
    // Get the location coordinates of user1 with their permission
    // Let us assume for this case that user1 allows the browser to get their location
    // Need to handle the case where user denies permission to use location
    navigator.geolocation.getCurrentPosition(function(location) {
      var lat = location.coords.latitude;
      var longg = location.coords.longitude;
      console.log('Lat = ' + lat + ' Long = ' + longg)

      // Fire an AJAX request to send user1's location, their message content and the base url values to the server
      $.getJSON('/outgoing', {
              textmsg: msgg,
              lat: lat,
              longg: longg,
              incoming_url: incoming_url
              }, function(data) {
                  alert("Your message has been sent!");
                  Debugging
                  console.log('Your message: ' + data.message);
            });
              return false;
    });

}

});
