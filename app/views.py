#Imports
from app import app
from app import db, models
from flask import Flask, jsonify, Response, request, g, render_template
import os
import twilio
from twilio.rest import TwilioRestClient
from twilio import twiml

#Get Twilio Account SID and Auth token from http://twilio.com/user/account
#Store them as environment variables TWILIO_ACCOUNT_SID and TWILIO_AUTH_TOKEN
#Store your user's numbers as environment variables USER1_NUM and USER2_NUM
#Note: User1/User2 could be Rider/Passenger, Order Requester/Delivery Person etc as per your use case
#Retrieve the stored values of the Account SID and Auth token
account_sid = os.environ.get('TWILIO_ACCOUNT_SID')
auth_token = os.environ.get('TWILIO_AUTH_TOKEN')
user1_num = os.environ.get('USER1_NUM')
user2_num = os.environ.get('USER2_NUM')

#Instantiate a new client object
client = TwilioRestClient(account_sid, auth_token)

#First route at / to render the app's html page in browser
@app.route('/', methods=['POST','GET'])
def first():
    return render_template('layout.html')

#For outgoing messages through the Twilio number
@app.route('/outgoing')
def outgoing():
    #Retrieve values passed through AJAX request on front end
    textmsg = request.args.get('textmsg')
    latitude = request.args.get('lat')
    longitude = request.args.get('longg')
    incoming_url = request.args.get('incoming_url')

    coords = (str(latitude) + ', ' + str(longitude))
    print(coords)

    #Search for a local phone number within a 100 mile radius of user's location
    #Can customize these parameters to your needs and perform more granular filtering based on several other attributes
    #See https://www.twilio.com/docs/api/rest/available-phone-numbers
    numbers = client.phone_numbers.search(near_lat_long=coords,
    distance='100',
    country='US',
    type='local',
    voice_enabled='true',
    sms_enabled='true'
    )
    #If no number found, you can do some of the following:
    # - Look for a number within a larger radius, repeat till a number is found
    # - Get a generic national number
    # - For countries where we don't have numbers,you can get a global number from a neighboring country.  

    #Purchase the first number in the search results
    if numbers:
        numb = numbers[0].purchase()

        #Print statements for debugging
        print('Number bought: ' + numb.phone_number)
        print('SID of number bought: ' + numb.sid)


        #Map the newly purchased number to the user pair for this conversation
        #Insert these values in the database to create an entry for persisting the mapping in the database
        #You could get these values from users through the UI
        #or potentially retrieve them from other tables in the database containing user info
        u = models.Mapping(user1=user1_num, user2=user2_num, twilionum=numb.phone_number)
        db.session.add(u)
        db.session.commit()

        #Can also implement sessions and maintain the mapping in the database only for the duration of a session between the two parties
        #This can help to recycle phone numbers and maintain a smaller pool of numbers to reuse across conversations

        #Configure the SMS request URL for the newly purchased number
        #Value of incoming_url is the base url followed by the /incoming route (see layout.js)
        #Thus for incoming sms arriving at our newly purchased number, we will ask Twilio to send webhooks to the /incoming route configured in our app
        number2 = client.phone_numbers.update(numb.sid, sms_url=incoming_url)
        #Print statements for debugging
        print('Incoming URL' + incoming_url)
        print('SMS url of number:' + number2.sms_url)

        #Forward message sent by user1 to user2 through the Twilio number associated with their conversation
        fwd = models.Mapping.query.filter_by(twilionum = numb.phone_number).first()

        message_user1 = client.messages.create(
        body=textmsg,
        to=fwd.user2,
        from_=numb.phone_number
        )

        #Send a confirmation message over the same Twilio number to the user sending the original message
        message_user2 = client.messages.create(
        body="Hey Bob your message has been sent to Alice. You can now directly reply to this message to communicate with Alice over this number.",
        mediaurl="http://i.imgur.com/DmPjhGo.jpg",
        to=fwd.user1,
        from_=numb.phone_number
        )

    else:
        print('error buying number')

    return jsonify(message=textmsg)


#For incoming messages to the Twilio number
@app.route('/incoming', methods=['POST','GET'])
def incoming():
    #Retrieve the values of the request parameters
    from_number = request.values.get('From', None)
    to_number = request.values.get('To', None)
    sms_body = request.values.get('Body', None)

    #Lookup on the Twilio forwarding number assigned to the conversation
    fwdnum = models.Mapping.query.filter_by(twilionum = to_number).first()
    #If from_number is the user1, send message to user2 and vice versa
    if from_number == fwdnum.user1:
        sendto = fwdnum.user2
    elif from_number == fwdnum.user2:
        sendto = fwdnum.user1
    else: return("Error!")

    #Forward the incoming message to the correct user
    message = client.messages.create(to=sendto, from_=to_number, body=sms_body)

    return ('All done')
