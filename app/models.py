from app import db

class Mapping(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user1 = db.Column(db.String(64), index=True, unique=True)
    user2 = db.Column(db.String(64), index=True, unique=True)
    twilionum = db.Column(db.String(64), index=True, unique=True)

    def __repr__(self):
        return '<Mapping %r>' % (self.twilionum)
