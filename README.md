Twilio-Anonymous-Communication-SMS
==================================

#### Twilio Anonymous Communication via Phone Number Masking

This app demonstrates an anonymous SMS communications flow between two parties via phone number masking by using a Twilio forwarding number for their conversation. A UI button click action by a user to communicate with another user triggers a Twilio REST API call to provision a phone number on the fly and use it for further offline communication between the two parties.

The app uses Python and Flask with the Flask-SQLAlchemy extension to manage a SQLite database. The database is used to store the mapping between a Twilio number and the phone numbers of the two users involved in a conversation over the Twilio number.

## Flow

![alt text](https://s3.amazonaws.com/twiliodemoimg/anon_comm_demo/flow.png "Flow")

## Pre-requisites
1. Development environment setup: Install Python, Flask and the twilio-python helper library. Follow instructions [here](https://www.twilio.com/docs/quickstart/python/devenvironment).
2. Flask-SQLAlchemy extension for Flask: Install with the command `pip install Flask-SQLAlchemy`
3. Twilio account, Twilio Account SID and Auth Token
4. Setup [ngrok](https://www.twilio.com/blog/2015/09/6-awesome-reasons-to-use-ngrok-when-testing-webhooks.html) to expose your application running locally to the internet. Optionally, you can deploy to Heroku or another hosting platform.

## Instructions
1. Clone the repository in your local environment.  
2. Create a virtual environment inside the project folder, instructions [here](https://www.twilio.com/docs/quickstart/python/devenvironment)  
3. Add your Twilio Account SID and Auth token as environment variables TWILIO_ACCOUNT_SID and TWILIO_AUTH_TOKEN. Also add your two user's numbers as USER1_NUM and USER2_NUM. Make sure to preserve the [E.164 number format](https://www.twilio.com/help/faq/phone-numbers/how-do-i-format-phone-numbers-to-work-internationally). We will be referencing these 4 values in views.py  
4. cd to the project folder
5. Activate the virtual environment: `source bin/activate`
6. Create the database: `./db_create.py`
7. Run the app: `./run.py`  
It should start the Flask server listening for requests on port 5000 (unless you changed the default port.)
8. In a separate terminal window, cd to the folder where you installed ngrok and run the command:  
`./ngrok http 5000`
9. ngrok will give you a secure public url where your app will be running, e.g. http://1a3f32ee.ngrok.io  
Paste that URL into a browser window to test the application.  

## Walkthrough

![alt text](https://s3.amazonaws.com/twiliodemoimg/anon_comm_demo/im1.png "Profile")

Click 'Send Text'  


![alt text](https://s3.amazonaws.com/twiliodemoimg/anon_comm_demo/im2.png "Prompt")

A window will pop up prompting you to submit your message. Type in your message and click OK.    


Allow the browser to use your location by clicking Allow when prompted. This will tell the app to provision a local Twilio number for your current location.  


![alt text](https://s3.amazonaws.com/twiliodemoimg/anon_comm_demo/im4.png "Confirm")

You will get a confirmation message of success.  


![alt text](https://s3.amazonaws.com/twiliodemoimg/anon_comm_demo/im5.png "Textmsg")
  

Alice will get the message sent by you (Bob) via Text through the newly provisioned Twilio number.      


Alice can directly reply to that number to text you (Bob) back.
